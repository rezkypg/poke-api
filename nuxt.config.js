export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Pokemon',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['@/assets/css/main.css'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/portal', mode: 'client' },
    { src: '~/plugins/vuex-persistedstate', mode: 'client' }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxt/postcss8',
    '@nuxtjs/composition-api/module',
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://go.nuxtjs.dev/stylelint
    '@nuxtjs/stylelint-module'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: ['@nuxtjs/axios', '@nuxt/image', '@nuxtjs/pwa'],
  compilerOptions: {
    types: ['@nuxt/types', '@nuxt/image']
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    postcss: {
      plugins: {
        tailwindcss: {},
        autoprefixer: {}
      }
    }
  },

  env: {
    apiUrl: process.env.API_URL || 'https://pokeapi.co/api/v2'
  },

  image: {
    domains: ['https://raw.githubusercontent.com']
  },

  pwa: {
    manifest: {
      name: 'Pokemon',
      display: 'standalone',
      lang: 'en'
    },
    icon: {
      fileName: 'logo-pokemon.png',
      sizes: [64, 120, 144, 152, 192, 384, 512],
      // The user agent is free to display the icon in any context.
      purpose: 'any'
    },
    workbox: {
      config: { debug: process.env.NODE_ENV === 'development' },
      runtimeCaching: [
        {
          urlPattern:
            'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/.*',
          handler: 'NetworkFirst',
          method: 'GET',
          strategyOptions: {
            cacheableResponse: { statuses: [200] },
            strategyOptions: {
              cacheName: 'pokemon-cache'
            }
          }
        }
      ]
    }
  }
}
