export const API_URL = 'https://pokeapi.co/api/v2'
export const BASE_POKEMON_SPRITE =
  'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon'

export const ARTWORK_POKEMON_SPRITE =
  'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork'
