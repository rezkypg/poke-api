import { ARTWORK_POKEMON_SPRITE, BASE_POKEMON_SPRITE } from '~/utils/constant'
import { getPokemonId } from '~/utils/pokemon'

export const getImage = (pokemonUrl: string): string =>
  `${BASE_POKEMON_SPRITE}/${getPokemonId(pokemonUrl)}.png`

export const getArtworkImage = (pokemonUrl: string): string =>
  `${ARTWORK_POKEMON_SPRITE}/${getPokemonId(pokemonUrl)}.png`
