// https://hidde.blog/using-javascript-to-trap-focus-in-an-element/

export const trapFocus = (element: any, action: string = 'addEvent') => {
  const focusableEls = element.querySelectorAll(
    'a[href]:not([disabled]), button:not([disabled]), textarea:not([disabled]), input[type="text"]:not([disabled]), input[type="radio"]:not([disabled]), input[type="checkbox"]:not([disabled]), select:not([disabled])'
  )
  const firstFocusableEl = focusableEls[0]
  const lastFocusableEl = focusableEls[focusableEls.length - 1]
  const KEYCODE_TAB = 9
  const keydownFunction = (e: any) => {
    const isTabPressed = e.key === 'Tab' || e.keyCode === KEYCODE_TAB

    if (!isTabPressed) {
      return
    }

    if (e.shiftKey) {
      /* shift + tab */ if (document.activeElement === firstFocusableEl) {
        lastFocusableEl.focus()
        e.preventDefault()
      }
    } /* tab */ else if (document.activeElement === lastFocusableEl) {
      firstFocusableEl.focus()
      e.preventDefault()
    }
  }

  if (action === 'addEvent') {
    element.addEventListener('keydown', keydownFunction)
  } else {
    element.removeEventListener('keydown', keydownFunction)
  }
}
