export const getPokemonId = (pokemonUrl: string): string => {
  if (!pokemonUrl) {
    return ''
  }
  const splittedPokemonUrl = pokemonUrl.split('/')
  return splittedPokemonUrl[splittedPokemonUrl.length - 2]
}
