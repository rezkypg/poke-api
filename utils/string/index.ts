export const capitalize = (value: string) => {
  return value[0].toUpperCase() + value.slice(1, value.length)
}

export const dashCaseToTitleCase = (value: string) => {
  const splittedString = value.split('-')

  return splittedString.map(capitalize).join(' ')
}

export const textToDashCase = (value: string) => {
  return value.toLowerCase().trim().replace(/\S+/gi, '-')
}
