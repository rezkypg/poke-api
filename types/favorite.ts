import type { Pokemon } from './pokemon'

export type FavoriteStateType = {
  favoriteList: Pokemon[]
}

export interface FavoriteStoreState {
  favorites: FavoriteStateType
}
