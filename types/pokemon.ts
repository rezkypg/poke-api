export type Pokemon = {
  name?: string
  url?: string
  teamNames?: (string | number)[]
}

export type PokemonAPIData = {
  results?: Pokemon[]
}

export type PokemonStateType = {
  pokemonList: Pokemon[]
  selectedPokemons: Pokemon
}

export interface PokemonStoreState {
  pokemon: PokemonStateType
}
