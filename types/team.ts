export type Team = {
  name: string
  pokemonIds: (string | number)[]
}

export type TeamStateType = {
  teamList: Team[]
  selectedTeam: string
}

export interface TeamStoreState {
  teams: TeamStateType
}
