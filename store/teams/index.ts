import type { TeamStateType, Team } from 'types/team'

export const state = (): TeamStateType => ({
  teamList: [],
  selectedTeam: ''
})

export const getters = {}

export const mutations = {
  REMOVE_TEAM(state: TeamStateType, teamName: string) {
    const filteredTeamList = state.teamList.filter(
      (team) => team.name !== teamName
    )
    state.teamList = [...filteredTeamList]
  },
  UPDATE_TEAM_NAME(
    state: TeamStateType,
    options: { oldName: string; newName: string }
  ) {
    state.teamList.map((team) => {
      if (team.name === options.oldName) {
        return (team.name = options.newName)
      }
      return team
    })
  },
  UPDATE_POKEMON_IDS(state: TeamStateType, team: Team) {
    const name = team.name
    const pokemonIds = team.pokemonIds
    state.teamList.map((team) => {
      if (team.name === name) {
        return (team.pokemonIds = [...pokemonIds])
      }
      return team
    })
  },
  SET_TEAM(state: TeamStateType, team: Team) {
    state.teamList.push(team)
  },
  SET_SELECTED_TEAM(state: TeamStateType, teamName: string) {
    state.selectedTeam = teamName
  }
}

export const actions = {}
