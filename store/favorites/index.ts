import type { Pokemon } from 'types/pokemon'
import type { FavoriteStateType } from 'types/favorite'

export const state = (): FavoriteStateType => ({
  favoriteList: []
})

export const getters = {}

export const mutations = {
  UPDATE_FAVORITE(state: FavoriteStateType, pokemon: Pokemon) {
    const pokemonIdx = state.favoriteList.indexOf(pokemon)
    if (pokemonIdx > -1) {
      state.favoriteList.splice(pokemonIdx, 1)
    } else {
      state.favoriteList.push(pokemon)
    }
  }
}

export const actions = {}
