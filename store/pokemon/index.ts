import type { PokemonStateType, Pokemon } from 'types/pokemon'

export const state = (): PokemonStateType => ({
  pokemonList: [],
  selectedPokemons: {}
})

export const getters = {
  getPokemonList(state: PokemonStateType) {
    return state.pokemonList
  }
}

export const mutations = {
  SYNC_TEAM_NAME(
    state: PokemonStateType,
    options: { oldName: string; newName: string }
  ) {
    state.pokemonList.map((pokemon) => {
      const pokemonTeamNameIdx = pokemon.teamNames
        ? pokemon.teamNames.indexOf(options.oldName)
        : -1
      if (pokemon.teamNames?.[pokemonTeamNameIdx]) {
        // This condition will be triggered when removing team
        // When the team is deleted, the teamName inside pokemon needed to be deleted too
        if (!options.newName) {
          pokemon.teamNames.splice(pokemonTeamNameIdx, 1)
          return pokemon
        }

        // This return value will be triggered when creating new team
        // When the new team is created, the teamName inside pokemon needed to be added too
        return (pokemon.teamNames[pokemonTeamNameIdx] = options.newName)
      }
      return pokemon
    })
  },
  SET_POKEMON_LIST(state: PokemonStateType, pokemonList: Pokemon[]) {
    state.pokemonList = [...(state.pokemonList || []), ...(pokemonList || [])]
  },
  SET_POKEMON_TEAM(
    state: PokemonStateType,
    options: { name: string; teamNames: (number | string)[] }
  ) {
    const { name, teamNames } = options
    const pokemonIdx: number = state.pokemonList.findIndex(
      (pokemon: Pokemon) => pokemon.name === name
    )

    if (pokemonIdx > -1) {
      state.pokemonList.splice(pokemonIdx, 1, {
        ...state.pokemonList[pokemonIdx],
        teamNames: [...teamNames]
      })
    }
  }
}

export const actions = {
  setPokemonList({ commit }: any, pokemonList: any) {
    commit('SET_POKEMON_LIST', pokemonList?.results)
  }
}
